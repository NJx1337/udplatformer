// Copyright Epic Games, Inc. All Rights Reserved.

#include "UdPlatformerGameMode.h"
#include "UdPlatformerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUdPlatformerGameMode::AUdPlatformerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
