// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UdPlatformerGameMode.generated.h"

UCLASS(minimalapi)
class AUdPlatformerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUdPlatformerGameMode();
};



