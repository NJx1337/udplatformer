// Copyright Epic Games, Inc. All Rights Reserved.

#include "UdPlatformer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UdPlatformer, "UdPlatformer" );
 